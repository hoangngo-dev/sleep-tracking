package edu.wpi.hmngo.sleeptracking;

import android.content.ContentValues;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by hmngo on 5/3/15.
 */
public class SleepRecord {
    private int id;
    private String date;
    private double duration; // minutes

    public SleepRecord(Date date, double duration) {
        this(-1, date, duration);
    }

    public SleepRecord(int id, Date date, double duration) {
        SimpleDateFormat formatter = new SimpleDateFormat("MM-dd");
        String dateStr = formatter.format(date);

        this.id = id;
        this.date = dateStr;
        this.duration = duration;
    }

    public SleepRecord(int id, String date, double duration) {
        this.id = id;
        this.date = date;
        this.duration = duration;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(DatabaseAttributes.SLEEP_RECORD_DATE, this.date);
        values.put(DatabaseAttributes.SLEEP_RECORD_DURATION, this.duration);

        return values;
    }
}
