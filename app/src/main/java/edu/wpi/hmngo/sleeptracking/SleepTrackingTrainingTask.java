package edu.wpi.hmngo.sleeptracking;

import android.content.Context;
import android.os.AsyncTask;

import java.util.List;

/**
 * Created by hmngo on 5/3/15.
 */
public class SleepTrackingTrainingTask extends AsyncTask<Context, Void, Void> {


    private static final double LEARNING_RATE = 0.01;
    private static final double ACCEPTABLE_DIFFERENCE = 10^-6;

    @Override
    protected Void doInBackground(Context... params) {
        DatabaseHandler database = new DatabaseHandler(params[0]);
        List<SleepSensorData> rawData = database.getAllSleepSensorData();
        int recordNum = 0;
        while (true) {
            SleepParameters sleepParameters = database.getSleepParameters();
            double lightDurationCoef = sleepParameters.getLightIntensityCoef();
            double phoneLockDurationCoef = sleepParameters.getPhoneLockDurationCoef();
            double phoneRechargingDurationCoef = sleepParameters.getPhoneRechargingDurationCoef();
            double phoneOffDurationCoef = sleepParameters.getPhoneOffDurationCoef();
            double stationaryDurationCoef = sleepParameters.getStationaryDurationCoef();
            double silenceDurationCoef = sleepParameters.getSilenceDurationCoef();

            recordNum %= rawData.size();
            SleepSensorData rawDatum = rawData.get(recordNum);
            SleepRecord record = database.getSleepRecordById(rawDatum.getId());
            double derivativeValue = lightDurationCoef * rawDatum.getLightIntensity()
                    + phoneLockDurationCoef * rawDatum.getPhoneLockDuration()
                    + phoneRechargingDurationCoef * rawDatum.getPhoneRechargingDuration()
                    + phoneOffDurationCoef * rawDatum.getPhoneOffDuration()
                    + stationaryDurationCoef * rawDatum.getSilenceDuration()
                    + silenceDurationCoef * rawDatum.getSilenceDuration()
                    - record.getDuration();

            double newLightDurationCoef = lightDurationCoef - LEARNING_RATE * derivativeValue * lightDurationCoef;
            double newPhoneLockDurationCoef = phoneLockDurationCoef - LEARNING_RATE * derivativeValue * phoneLockDurationCoef;
            double newPhoneRechargingDurationCoef = phoneRechargingDurationCoef - LEARNING_RATE * derivativeValue * phoneRechargingDurationCoef;
            double newPhoneOffDurationCoef = phoneOffDurationCoef - LEARNING_RATE * derivativeValue * phoneOffDurationCoef;
            double newStationaryDurationCoef = stationaryDurationCoef - LEARNING_RATE * derivativeValue * stationaryDurationCoef;
            double newSilenceDurationCoef = silenceDurationCoef - LEARNING_RATE * derivativeValue * silenceDurationCoef;

            if (isConvergence(lightDurationCoef, newLightDurationCoef) &&
                isConvergence(phoneLockDurationCoef, newPhoneLockDurationCoef) &&
                isConvergence(phoneRechargingDurationCoef, newPhoneRechargingDurationCoef) &&
                isConvergence(phoneOffDurationCoef, newPhoneOffDurationCoef) &&
                isConvergence(stationaryDurationCoef, newStationaryDurationCoef) &&
                isConvergence(silenceDurationCoef, newSilenceDurationCoef)) break;
            SleepParameters newSleepParams = new SleepParameters(lightDurationCoef,
                    phoneLockDurationCoef, phoneRechargingDurationCoef, phoneOffDurationCoef,
                    stationaryDurationCoef, silenceDurationCoef);
            database.updateSleepGuessingParams(newSleepParams);

            recordNum++;
        }

        database.close();
        return null;
    }

    private boolean isConvergence(double alpha, double theta) {
        return Math.abs(alpha - theta) <= ACCEPTABLE_DIFFERENCE;
    }

}
