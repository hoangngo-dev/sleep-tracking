package edu.wpi.hmngo.sleeptracking;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.ContactsContract;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hmngo on 5/3/15.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "sleeptrackingdb";
    private static final int DATABASE_VERSION = 1;

    private static final String TABLE_SLEEP_SENSOR_DATA = "sleepsensordata";
    private static final String TABLE_SLEEP_RECORD = "sleeprecords";
    private static final String TABLE_SLEEP_GUESSING_PARAMS = "sleepguessingparams";


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createSleepSensorDataTableSQL = "CREATE TABLE " + TABLE_SLEEP_SENSOR_DATA + "("
                + "id" + " INTEGER PRIMARY KEY, "
                + DatabaseAttributes.SLEEP_SENSOR_DATA_LIGHT_INTENSITY_DURATION + " DOUBLE NOT NULL"
                + DatabaseAttributes.SLEEP_SENSOR_DATA_PHONE_LOCK_DURATION + " DOUBLE NOT NULL"
                + DatabaseAttributes.SLEEP_SENSOR_DATA_PHONE_RECHARGING_DURATION + " DOUBLE NOT NULL"
                + DatabaseAttributes.SLEEP_SENSOR_DATA_PHONE_OFF_DURATION + " DOUBLE NOT NULL"
                + DatabaseAttributes.SLEEP_SENSOR_DATA_STATIONARY_DURATION  + " DOUBLE NOT NULL"
                + DatabaseAttributes.SLEEP_SENSOR_DATA_SILENCE_DURATION + " DOUBLE NOT NULL"
                + ")";
        String createSleepRecordTableSQL = "CREATE TABLE " + TABLE_SLEEP_RECORD + "("
                + "id" + " INTEGER PRIMARY KEY, "
                + DatabaseAttributes.SLEEP_RECORD_DURATION + " DOUBLE NOT NULL"
                + DatabaseAttributes.SLEEP_RECORD_DATE + " STRING NOT NULL"
                + ")";
        String createSleepGuessingParametersTableSQL= "CREATE TABLE " + TABLE_SLEEP_GUESSING_PARAMS + "("
                + "id" + " INTEGER PRIMARY KEY, "
                + DatabaseAttributes.LIGHT_INTENSITY_COEF + " DOUBLE NOT NULL DEFAULT 1"
                + DatabaseAttributes.PHONE_LOCK_DURATION_COEF + " DOUBLE NOT NULL DEFAULT 1"
                + DatabaseAttributes.PHONE_RECHARGING_DURATION_COEF + " DOUBLE NOT NULL DEFAULT 1"
                + DatabaseAttributes.PHONE_OFF_DURATION_COEF + " DOUBLE NOT NULL DEFAULT 1"
                + DatabaseAttributes.STATIONARY_DURATION_COEF + " DOUBLE NOT NULL DEFAULT 1"
                + DatabaseAttributes.SILENCE_DURATION_COEF + " DOUBLE NOT NULL DEFAULT 1"
                + ")";
        String insertEntryInSleepGuessingParamsTableSQL = "INSERT INTO " + TABLE_SLEEP_GUESSING_PARAMS
                + " VALUES (0, 1, 1, 1, 1, 1, 1)";

        db.execSQL(createSleepSensorDataTableSQL);
        db.execSQL(createSleepRecordTableSQL);
        db.execSQL(createSleepGuessingParametersTableSQL);
        db.execSQL(insertEntryInSleepGuessingParamsTableSQL);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXIST " + TABLE_SLEEP_RECORD);
        db.execSQL("DROP TABLE IF EXIST " + TABLE_SLEEP_SENSOR_DATA);
        db.execSQL("DROP TABLE IF EXIST " + TABLE_SLEEP_GUESSING_PARAMS);
    }

    public Cursor fetchBySelection(String table, String[] selections, String[] selectionArgs) {
        SQLiteDatabase database = this.getReadableDatabase();
        String selectionQuery = "";
        for (int i = 0; i < selections.length - 1; i++) {
            selectionQuery += selections[i] + "=? AND ";
        }
        selectionQuery += selections[selections.length - 1] + "=?";

        Cursor cursor = database.query(table, null, selectionQuery, selectionArgs, null, null, null,
                null);
        return cursor;
    }

    public void createSleepSensorData(SleepSensorData sleepSensorData) {
        SQLiteDatabase database = this.getReadableDatabase();
        database.insert(TABLE_SLEEP_SENSOR_DATA, null, sleepSensorData.toContentValues());
        database.close();
    }

    public SleepSensorData getSleepSensorDataById(int id) {
        SleepSensorData sleepSensorData = null;
        Cursor cursor = fetchBySelection(TABLE_SLEEP_SENSOR_DATA,
                new String[]{"id"},
                new String[]{Integer.toString(id)});
        if (isNonEmptyCursor(cursor)) {
            sleepSensorData = new SleepSensorData(cursor.getDouble(1), cursor.getDouble(2),
                    cursor.getDouble(3), cursor.getDouble(4), cursor.getDouble(5), cursor.getDouble(6));
        }
        return sleepSensorData;
    }

    public void createSleepRecord(SleepRecord sleepRecord) {
        SQLiteDatabase database = this.getReadableDatabase();
        database.insert(TABLE_SLEEP_RECORD, null, sleepRecord.toContentValues());
        database.close();
    }

    public SleepRecord getSleepRecordById(int id) {
        SleepRecord sleepRecord = null;
        Cursor cursor = this.fetchBySelection(TABLE_SLEEP_RECORD,
                new String[]{"id"},
                new String[]{Integer.toString(id)});
        if (isNonEmptyCursor(cursor)) {
            sleepRecord = new SleepRecord(cursor.getInt(0), cursor.getString(1), cursor.getDouble(2));
        }

        return sleepRecord;
    }

    public List<SleepRecord> getAllSleepRecords() {
        List<SleepRecord> sleepRecords = new ArrayList<>();
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery("SELECT * FROM " + TABLE_SLEEP_RECORD, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();

            do {
                SleepRecord sleepRecord = this.getSleepRecordById(cursor.getInt(0));
                if (sleepRecord != null) {
                    sleepRecords.add(sleepRecord);
                }
            } while (cursor.moveToNext());
        }

        return sleepRecords;
    }

    public List<SleepSensorData> getAllSleepSensorData() {
        List<SleepSensorData> sensorData = new ArrayList<>();
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery("SELECT * FROM " + TABLE_SLEEP_SENSOR_DATA, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();

            do {
                SleepSensorData sleepSensorData = this.getSleepSensorDataById(cursor.getInt(0));
                if (sleepSensorData != null) {
                    sensorData.add(sleepSensorData);
                }
            } while (cursor.moveToNext());
        }

        return sensorData;
    }

    public void updateSleepGuessingParams(SleepParameters sleepParameters) {
        SQLiteDatabase database = this.getReadableDatabase();
        database.update(TABLE_SLEEP_GUESSING_PARAMS, sleepParameters.toContentValues(), "id=0", null);
        database.close();
    }

    public SleepParameters getSleepParameters() {
        SleepParameters sleepParameters = null;
        Cursor cursor = fetchBySelection(TABLE_SLEEP_GUESSING_PARAMS,
                new String[]{"id"},
                new String[]{"0"});
        if (isNonEmptyCursor(cursor)) {
            cursor.moveToFirst();
            sleepParameters = new SleepParameters(cursor.getDouble(1), cursor.getDouble(2),
                    cursor.getDouble(3), cursor.getDouble(4), cursor.getDouble(5), cursor.getDouble(6));
        }

        return sleepParameters;
    }

    private boolean isNonEmptyCursor(Cursor cursor) {
        return cursor != null && cursor.getCount() > 0;
    }

}
