package edu.wpi.hmngo.sleeptracking;

/**
 * Created by hmngo on 5/3/15.
 */
public class DatabaseAttributes {
    public static final String SLEEP_SENSOR_DATA_LIGHT_INTENSITY_DURATION = "light_intensity_duration";
    public static final String SLEEP_SENSOR_DATA_PHONE_LOCK_DURATION = "phone_lock_duration";
    public static final String SLEEP_SENSOR_DATA_PHONE_RECHARGING_DURATION = "phone_recharging_duration";
    public static final String SLEEP_SENSOR_DATA_PHONE_OFF_DURATION = "phone_off_duration";
    public static final String SLEEP_SENSOR_DATA_STATIONARY_DURATION = "stationary_duration";
    public static final String SLEEP_SENSOR_DATA_SILENCE_DURATION = "silence_duration";

    public static final String SLEEP_RECORD_DURATION = "duration";
    public static final String SLEEP_RECORD_DATE = "date";

    public static final String LIGHT_INTENSITY_COEF = "light_intensity_coef";
    public static final String PHONE_LOCK_DURATION_COEF = "phone_lock_duration_coef";
    public static final String PHONE_RECHARGING_DURATION_COEF = "phone_recharging_duration_coef";
    public static final String PHONE_OFF_DURATION_COEF = "phone_off_duration_coef";
    public static final String STATIONARY_DURATION_COEF = "stationary_duration_coef";
    public static final String SILENCE_DURATION_COEF = "silence_duration_coef";
}
