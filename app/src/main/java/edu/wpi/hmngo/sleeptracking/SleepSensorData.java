package edu.wpi.hmngo.sleeptracking;

import android.content.ContentValues;

/**
 * Created by hmngo on 5/3/15.
 */
public class SleepSensorData {
    private int id;
    private double lightIntensity;
    private double phoneLockDuration;
    private double phoneRechargingDuration;
    private double phoneOffDuration;
    private double stationaryDuration;
    private double silenceDuration;

    public SleepSensorData(double lightIntensity, double phoneLockDuration,
                           double phoneRechargingDuration, double phoneOffDuration,
                           double stationaryDuration, double silenceDuration) {
        this(-1, lightIntensity, phoneLockDuration, phoneRechargingDuration, phoneOffDuration,
                stationaryDuration, silenceDuration);
    }

    public SleepSensorData(int id, double lightIntensity, double phoneLockDuration,
                           double phoneRechargingDuration, double phoneOffDuration,
                           double stationaryDuration, double silenceDuration) {
        this.id = id;
        this.lightIntensity = lightIntensity;
        this.phoneLockDuration = phoneLockDuration;
        this.phoneRechargingDuration = phoneRechargingDuration;
        this.phoneOffDuration = phoneOffDuration;
        this.stationaryDuration = stationaryDuration;
        this.silenceDuration = silenceDuration;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLightIntensity() {
        return lightIntensity;
    }

    public void setLightIntensity(double lightIntensity) {
        this.lightIntensity = lightIntensity;
    }

    public double getPhoneLockDuration() {
        return phoneLockDuration;
    }

    public void setPhoneLockDuration(double phoneLockDuration) {
        this.phoneLockDuration = phoneLockDuration;
    }

    public double getPhoneRechargingDuration() {
        return phoneRechargingDuration;
    }

    public void setPhoneRechargingDuration(double phoneRechargingDuration) {
        this.phoneRechargingDuration = phoneRechargingDuration;
    }

    public double getPhoneOffDuration() {
        return phoneOffDuration;
    }

    public void setPhoneOffDuration(double phoneOffDuration) {
        this.phoneOffDuration = phoneOffDuration;
    }

    public double getStationaryDuration() {
        return stationaryDuration;
    }

    public void setStationaryDuration(double stationaryDuration) {
        this.stationaryDuration = stationaryDuration;
    }

    public double getSilenceDuration() {
        return silenceDuration;
    }

    public void setSilenceDuration(double silenceDuration) {
        this.silenceDuration = silenceDuration;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(DatabaseAttributes.SLEEP_SENSOR_DATA_LIGHT_INTENSITY_DURATION, this.lightIntensity);
        values.put(DatabaseAttributes.SLEEP_SENSOR_DATA_PHONE_LOCK_DURATION, this.phoneLockDuration);
        values.put(DatabaseAttributes.SLEEP_SENSOR_DATA_PHONE_RECHARGING_DURATION, this.phoneRechargingDuration);
        values.put(DatabaseAttributes.SLEEP_SENSOR_DATA_PHONE_OFF_DURATION, this.phoneOffDuration);
        values.put(DatabaseAttributes.SLEEP_SENSOR_DATA_STATIONARY_DURATION, this.stationaryDuration);
        values.put(DatabaseAttributes.SLEEP_SENSOR_DATA_SILENCE_DURATION, this.silenceDuration);

        return values;
    }
}
