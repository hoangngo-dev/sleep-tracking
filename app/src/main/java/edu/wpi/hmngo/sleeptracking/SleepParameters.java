package edu.wpi.hmngo.sleeptracking;

import android.content.ContentValues;

/**
 * Created by hmngo on 5/3/15.
 */
public class SleepParameters {
    private int id;
    private double lightIntensityCoef;
    private double phoneLockDurationCoef;
    private double phoneRechargingDurationCoef;
    private double phoneOffDurationCoef;
    private double stationaryDurationCoef;
    private double silenceDurationCoef;

    public SleepParameters(double lightIntensityCoef, double phoneLockDurationCoef,
                           double phoneRechargingDurationCoef, double phoneOffDurationCoef,
                           double stationaryDurationCoef, double silenceDurationCoef) {
        this(0, lightIntensityCoef, phoneLockDurationCoef, phoneRechargingDurationCoef,
                phoneOffDurationCoef, stationaryDurationCoef, silenceDurationCoef);
    }

    private SleepParameters(int id, double lightIntensityCoef, double phoneLockDurationCoef,
                           double phoneRechargingDurationCoef, double phoneOffDurationCoef,
                           double stationaryDurationCoef, double silenceDurationCoef) {
        this.id = id;
        this.lightIntensityCoef = lightIntensityCoef;
        this.phoneLockDurationCoef = phoneLockDurationCoef;
        this.phoneRechargingDurationCoef = phoneRechargingDurationCoef;
        this.phoneOffDurationCoef = phoneOffDurationCoef;
        this.stationaryDurationCoef = stationaryDurationCoef;
        this.silenceDurationCoef = silenceDurationCoef;
    }

    public double getLightIntensityCoef() {
        return lightIntensityCoef;
    }

    public void setLightIntensityCoef(double lightIntensityCoef) {
        this.lightIntensityCoef = lightIntensityCoef;
    }

    public double getPhoneLockDurationCoef() {
        return phoneLockDurationCoef;
    }

    public void setPhoneLockDurationCoef(double phoneLockDurationCoef) {
        this.phoneLockDurationCoef = phoneLockDurationCoef;
    }

    public double getPhoneRechargingDurationCoef() {
        return phoneRechargingDurationCoef;
    }

    public void setPhoneRechargingDurationCoef(double phoneRechargingDurationCoef) {
        this.phoneRechargingDurationCoef = phoneRechargingDurationCoef;
    }

    public double getPhoneOffDurationCoef() {
        return phoneOffDurationCoef;
    }

    public void setPhoneOffDurationCoef(double phoneOffDurationCoef) {
        this.phoneOffDurationCoef = phoneOffDurationCoef;
    }

    public double getStationaryDurationCoef() {
        return stationaryDurationCoef;
    }

    public void setStationaryDurationCoef(double stationaryDurationCoef) {
        this.stationaryDurationCoef = stationaryDurationCoef;
    }

    public double getSilenceDurationCoef() {
        return silenceDurationCoef;
    }

    public void setSilenceDurationCoef(double silenceDurationCoef) {
        this.silenceDurationCoef = silenceDurationCoef;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(DatabaseAttributes.LIGHT_INTENSITY_COEF, this.lightIntensityCoef);
        values.put(DatabaseAttributes.PHONE_LOCK_DURATION_COEF, this.phoneLockDurationCoef);
        values.put(DatabaseAttributes.PHONE_RECHARGING_DURATION_COEF, this.phoneRechargingDurationCoef);
        values.put(DatabaseAttributes.PHONE_OFF_DURATION_COEF, this.phoneOffDurationCoef);
        values.put(DatabaseAttributes.STATIONARY_DURATION_COEF, this.stationaryDurationCoef);
        values.put(DatabaseAttributes.SILENCE_DURATION_COEF, this.silenceDurationCoef);

        return values;
    }
}
